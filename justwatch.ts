import fetch from 'cross-fetch';

declare global {
    interface Date {
        getWeek(): number;
    }
}

// Source: https://weeknumber.com/how-to/javascript
Date.prototype.getWeek = function () {
    var date = new Date(this.getTime());
    date.setHours(0, 0, 0, 0);
    // Thursday in current week decides the year.
    date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
    // January 4 is always in week 1.
    var week1 = new Date(date.getFullYear(), 0, 4);
    // Adjust to Thursday in week 1 and count number of weeks from date to week1.
    return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
        - 3 + (week1.getDay() + 6) % 7) / 7);
}

// Initial work in Python by dawoudt : https://github.com/dawoudt/JustWatchAPI
export class JustWatch
{
    api_base_template = (path:string) => `https://apis.justwatch.com/content/${path}`;
    kwargs: any = {};
    country: string = 'AU';
    kwargs_cinema: Array<object> = [];
    locale: string = '';

    constructor()
    {
        
    }
    async init(country: string, kwargs: any = {})
    {
		this.kwargs = kwargs
		this.country = country
        this.kwargs_cinema = []
        await this.set_locale()
    }

    async set_locale()
    {
		let warning = (country: string) => `\nWARN: Unable to locale for ${country}! Defaulting to en_AU\n`;
		let default_locale = 'en_AU'
		let path = 'locales/state'
		let api_url = this.api_base_template(path)
        await fetch(api_url)
            .then(response => {
                if (!response.ok) {throw Error(warning(this.country));}
                return response.json();
            })
            .then(json => 
                {
                    for (const i in json)
                    {
                        if (json[i].country == this.country || json[i].iso_3166_2 == this.country)
                        {
                            this.locale = <string>json[i].full_locale
                            break
                        }
                    }
                })
            .catch(error => 
                {
                    console.log(error);
                    this.locale = default_locale
                });
    }

    async search_for_item(query:string , kwargs: any = {}){

        let path = `titles/${this.locale}/popular`
        let api_url = this.api_base_template(path)

        if(kwargs)
            this.kwargs = kwargs
        if(query)
            this.kwargs.query = query

        let payload: any = {
            "age_certifications":null,
            "content_types":null,
            "presentation_types":null,
            "providers":null,
            "genres":null,
            "languages":null,
            "release_year_from":null,
            "release_year_until":null,
            "monetization_types":null,
            "min_price":null,
            "max_price":null,
            "nationwide_cinema_releases_only":null,
            "scoring_filter_types":null,
            "cinema_release":null,
            "query":null,
            "page":null,
            "page_size":null,
            "timeline_type":null,
            "person_id":null
        }

        for (const key in this.kwargs)
        {
            let value = this.kwargs[key]
            if(key in payload)
                payload[key] = value
            else
                console.log(`${key} is not a valid keyword`)

        }
        const request = (async () => {
            const rawResponse = await fetch(api_url, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'User-Agent': 'JustWatch client (github.com/dawoudt/JustWatchAPI)'
                },
                body: JSON.stringify(payload)
            })
            const content = await rawResponse.json().catch(error => {
                console.log(error);
                return {}
            })
            return content
        })
        
        let results: any = await request();
        return results
    }

    async get_providers()
    {
        let path = `providers/locale/${this.locale}`
        let api_url = this.api_base_template(path)
        const request = (async () => {
            const rawResponse = await fetch(api_url, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'User-Agent': 'JustWatch client (github.com/dawoudt/JustWatchAPI)'
                }
            })
            const content = await rawResponse.json().catch(error => {
                console.log(error);
                return {}
            })
            return content
        })
        let results: any = await request();
        return results
    }

    async get_title(title_id: number, content_type:string ="movie")
    {
        let path = `titles/${content_type}/${title_id}/locale/${this.locale}`
        let api_url = this.api_base_template(path)
        const request = (async () => {
            const rawResponse = await fetch(api_url, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'User-Agent': 'JustWatch client (github.com/dawoudt/JustWatchAPI)'
                }
            })
            const content = await rawResponse.json().catch(error => {
                console.log(error);
                return {}
            })
            return content
        })
        let results: any = await request();
        return results
    }

    /*
    Returns a dictionary of titles returned
        from search and their respective ID's
        >>> ...
        >>> just_watch.get_title_id('The Matrix')
        {'The Matrix': 10, ... }
    */
    async search_title_id(query:string)
    {
        let results:any = await this.search_for_item(query)
        //console.log(results)
        let new_r: any = {}
        for (const el in results.items)
        {
            let value = results.items[el]
            new_r[value.id] = value.title
        }
        return new_r
    }

    async get_genres()
    {
        let path = `genres/locale/${this.locale}`
        let api_url = this.api_base_template(path)
        const request = (async () => {
            const rawResponse = await fetch(api_url, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'User-Agent': 'JustWatch client (github.com/dawoudt/JustWatchAPI)'
                }
            })
            const content = await rawResponse.json().catch(error => {
                console.log(error);
                return {}
            })
            return content
        })
        let results: any = await request();
        return results
    }

    //TODO: untested
    async get_season(season_id: number)
    {
        let path = `titles/show_season/${season_id}/locale/${this.locale}`
        let api_url = this.api_base_template(path)
        const request = (async () => {
            const rawResponse = await fetch(api_url, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'User-Agent': 'JustWatch client (github.com/dawoudt/JustWatchAPI)'
                }
            })
            const content = await rawResponse.json().catch(error => {
                console.log(error);
                return {}
            })
            return content
        })
        let results: any = await request();
        return results
    }

    //TODO: untested
    async get_person_detail(person_id: number)
    {
        let path = `people/${person_id}/locale/${this.locale}`
        let api_url = this.api_base_template(path)
        const request = (async () => {
            const rawResponse = await fetch(api_url, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'User-Agent': 'JustWatch client (github.com/dawoudt/JustWatchAPI)'
                }
            })
            const content = await rawResponse.json().catch(error => {
                console.log(error);
                return {}
            })
            return content
        })
        let results: any = await request();
        return results
    }

    //TODO: untested
    async get_cinema_times(title_id: number, content_type:string ="movie", kwargs:any = {})
    {
        let payload: any = {
            "date": null,
            "latitude": null,
            "longitude": null,
            "radius": 20000
        }
        for (const key in kwargs)
        {
            if (key in payload)
            {
                payload[key] = kwargs[key]
            }
            else
            {
                console.log(`${key} is not a valid keyword`)
            }
        }
        let path = `titles/${content_type}/${title_id}/showtimes`
        let api_url = this.api_base_template(path)
        const request = (async () => {
            const rawResponse = await fetch(api_url, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'User-Agent': 'JustWatch client (github.com/dawoudt/JustWatchAPI)'
                },
                body: JSON.stringify(payload)
            })
            const content = await rawResponse.json().catch(error => {
                console.log(error);
                return {}
            })
            return content
        })
        let results: any = await request();
        return results
    }

    async get_cinema_details(kwargs:any = {})
    {
        let payload: any = {
            "latitude": null,
            "longitude": null,
            "radius": 20000
        }
        for (const key in kwargs)
        {
            if (key in payload)
            {
                payload[key] = kwargs[key]
            }
            else
            {
                console.log(`${key} is not a valid keyword`)
            }
        }
        let path = `cinemas/${this.locale}`
        let api_url = this.api_base_template(path)
        const request = (async () => {
            const rawResponse = await fetch(api_url, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'User-Agent': 'JustWatch client (github.com/dawoudt/JustWatchAPI)'
                },
                body: JSON.stringify(payload)
            })
            const content = await rawResponse.json().catch(error => {
                console.log(error);
                return {}
            })
            return content
        })
        let results: any = await request();
        return results
    }

    //TODO: untested
    async get_upcoming_cinemas(weeks_offset: number, nationwide_cinema_releases_only: boolean = true)
    {
        let payload = {
            'nationwide_cinema_releases_only': nationwide_cinema_releases_only,
            'body': {}
        }
        let now_date: Date = new Date()
        now_date.setDate(now_date.getDate() + (weeks_offset * 7))
        let year: number = now_date.getFullYear()
        let week: number = now_date.getWeek()
        let api_url = `https://apis.justwatch.com/content/titles/movie/upcoming/${year}/${week}/locale/${this.locale}`
        const request = (async () => {
            const rawResponse = await fetch(api_url, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'User-Agent': 'JustWatch client (github.com/dawoudt/JustWatchAPI)'
                }
            })
            const content = await rawResponse.json().catch(error => {
                console.log(error);
                return {}
            })
            return content
        })
        let results: any;
        try
        {
            results = await request();
        }
        catch (error)
        {
            return { 'page': 0, 'page_size': 0, 'total_pages': 1, 'total_results': 0, 'items': [] }
        }
        return results
    }

    //TODO: untested --> not sure if this works
    async get_certifications(content_type = "movie")
    {
        let payload: any = { 'country': this.country, 'object_type': content_type }
        let api_url = 'https://apis.justwatch.com/content/age_certifications'
        const request = (async () => {
            const rawResponse = await fetch(api_url, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'User-Agent': 'JustWatch client (github.com/dawoudt/JustWatchAPI)'
                },
                body: JSON.stringify(payload)
            })
            const content = await rawResponse.json().catch(error => {
                console.log(error);
                return {}
            })
            return content
        })
        let results: any = await request();
        return results
    }
}
