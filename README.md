# JustWatch API

Unofficial JustWatch API in TypeScript.  
https://justwatch.com  

Fully inspired by dawoudt's JustWatch API in Python (https://github.com/dawoudt/JustWatchAPI).  
Possibly full of bugs.  
Not all methods tested.  
Documentation to come (please check https://github.com/dawoudt/JustWatchAPI , I tried to make my API as similar as possible to this one)

# Licence

MIT Licence
